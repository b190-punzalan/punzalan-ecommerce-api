import {Card} from 'react-bootstrap';


export default function Quote() {
	return (
		<Card>
	      <Card.Header>Why you should start learning today!</Card.Header>
	      <Card.Body>
	        <blockquote className="blockquote mb-0">
	          <p>
	            {' '}
	            “The single biggest difference between financial success and financial failure is how well you manage your money. It’s simple: to master money, you must manage money.”

.{' '}
	          </p>
	          <footer className="blockquote-footer">
	            T Harv Eker, motivational speaker
	          </footer>
	        </blockquote>
	      </Card.Body>
	    </Card>
	    
	);
}
