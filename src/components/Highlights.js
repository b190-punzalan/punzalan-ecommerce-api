import {Row, Col, Card} from 'react-bootstrap';


export default function Highlights() {

	return (
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Learn from Home</h2>
						</Card.Title>
						<Card.Text>
							Moms are ever busy so we understand how important it is to grow your mind and spirit without sacrificing time for the kids and the hubby! All our courses can be accessed from the comforts of your home (bedroom, bathroom, kitchen, wherever you are!) so you can still be the super mom that you are while learning.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Meet other Raketera Moms!</h2>
						</Card.Title>
						<Card.Text>
							Cheer up! You are not in this journey alone. We are a community full of moms lifting moms! We got you and we are here for you, momma!
							
						</Card.Text>

						<Card.Text>
							Can't wait to meet you!
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Not a Mom? Not a Problem!</h2>
						</Card.Title>
						<Card.Text>
							Learning is for everyone. Our community is open to anyone who wishes to learn and grow by better managing their finances. Feel free to join us and let us all learn from one another.  
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	);
}


// export default function Highlights() {
//   return (
//   	<>
//     <Card>
//         <Card.Img variant="top" src="./raketera.png/200px300" />
//         <Card.Body>
//           <Card.Text>
//             Some quick example text to build on the card title and make up the
//             bulk of the card's content.
//           </Card.Text>
//         </Card.Body>
//     </Card>
//     {/*  <br />
//     <Card>
//         <Card.Body>
//           <Card.Text>
//             Some quick example text to build on the card title and make up the
//             bulk of the card's content.
//           </Card.Text>
//         </Card.Body>
//         <Card.Img variant="bottom" src="holder.js/100px180" />
//     </Card>*/}
//     </>
//   );
// }

