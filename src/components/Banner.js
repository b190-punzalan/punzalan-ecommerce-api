// react permits the use of object literals if the dev will imort multiple components from the same dependency
import {Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function Banner({data}){

	const {title, content, destination, label} = data;
	return(
		<Row className="banner">
			<Col className="p-5">
				{/*<h1>{title}</h1>
				<p>{content}</p>*/}
				{/*<Link to={destination}>{label}</Link>*/}
				<Link className='btn btn-primary' to={destination}>Learn More</Link>
			</Col>
		</Row>

	)
}
