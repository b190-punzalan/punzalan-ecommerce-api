import {Row, Col, Card, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';



// we can try to deconstruct the props object received from the parent component (Courses.js) so that the main object from the mock database will be accessed
export default function CourseCard({courseProp}){

	console.log(courseProp);
	console.log(typeof courseProp);

	const {name, _id} = courseProp;

	
	return(
		
				<Card className="courseCard">
		      		
		            <Card.Body className="cardBody">
		                <Card.Title className="courseName">{name}</Card.Title>
		                		                
		                <Link className='btn btn-primary' to={`/courses/${_id}`}>Learn More</Link>
		           
		            </Card.Body>
		        </Card>
	)
}