const coursesData=[
	{
		id: "pera01",
		name: "Barya Pa More",
		description: "Who says you need millions to start investing? Great things start from small beginnings! It's the same with savings and investment. It starts with the first peso! Find out how you can start building your millions even with very little amounts!",
		price: 1500,
		duration: "4 hours"
		onOffer: true
	},
	{
		id: "pera02",
		name: "Your Sana All Guide to Investing",
		description: "Everyone can do it! You just need to start. Learn how to start investing and know more about your investment options.",
		price: 2500,
		duration: "16 hours"
		onOffer: true
	},
	{
		id: "pera03",
		name: "Pera-Paraan, Yes to Extra Income",
		description: "Learn how to create multiple streams of income so you can have more to save and invest",
		price: 2000,
		duration: "8 hours"
		onOffer: true
	},
	{
		id: "pera04",
		name: "6 Steps to a Solid Financial Foundation",
		description: "Build your finances the right way with this course.",
		price: 1500,
		duration: "16 hours"
		onOffer: true
	},
	{
		id: "pera05",
		name: "Investing for the Wary Investor",
		description: "Scared to begin investing? Here is a course to help you find out more about investment options for the  conservative investor.",
		price: 1500,
		duration: "8 hours"
		onOffer: true
	},
	{
		id: "peraLibre",
		name: "Ask Me Anything!",
		description: "Get an hour of one-on-one convo with me so you can ask me anything about my investment journey and how I can help you get started with yours.",
		price: "Free!",
		duration: "1 hour"
		onOffer: true
	}
]

export default coursesData;