
import './App.css';

// UserContext
import {UserProvider} from './UserContext.js';


import {useEffect, useState} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
// import background from '/.components/raketera.png';


// Web Components
import AppNavbar from './AppNavbar';

// Web Pages
import Home from "./Pages/Home";
import Courses from "./Pages/Courses";
import CourseView from './components/CourseView';
import Register from "./Pages/Register";
import Login from "./Pages/Login";
import Logout from "./Pages/Logout";
import Error from "./Pages/Error";
import Checkout from "./Pages/Checkout";


function App() {

  const [user, setUser] = useState ({
    id: null,
    isAdmin: null
    });

  const unsetUser = ()=>{localStorage.clear();
  }

  
  useEffect(() => {


  fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
    headers: {
      Authorization: `Bearer ${ localStorage.getItem('token') }`
    }
  })
  .then(res => res.json())
  .then(data => {

    // Set the user states values with the user details upon successful login.
    if (typeof data._id !== "undefined") {

      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      });

    } else {

      setUser({
        id: null,
        isAdmin: null
      });

    }

  })

  }, []);


return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container fluid>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/courses' element={<Courses />} />
          <Route path='/courses/:courseId' element={<CourseView />} />
          <Route path='/login' element={<Login />} />
          <Route path='/logout' element={<Logout />} />
          <Route path='/register' element={<Register />} />
          <Route path='/myCourses/:id' element={<Checkout />} />
          <Route path='*' element={<Error />} />
        </Routes>
        </Container>
      </Router>
    </UserProvider>
    </>
  );
}



export default App;

