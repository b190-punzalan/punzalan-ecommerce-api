import { Fragment, useEffect, useState, useContext } from 'react';
import AdminView from '../components/AdminView';
// import coursesData from '../data/coursesData';
import UserView from '../components/UserView';
import UserContext from '../UserContext';



export default function Checkout(){
	
	const { user } = useContext(UserContext);

	const [checkout, setCheckout] = useState([]);

	const fetchData = () => {

        fetch(`${ process.env.REACT_APP_API_URL}/myCourses`)
        .then(res => res.json())
        .then(data => {

            setCheckout(data);

        });

    }


	useEffect(() => {
        fetchData();
    }, []);
    
    return (
    	
        <Fragment>
            {/* If the user is an admin, show the Admin View. If not, show the regular courses page. */}
            {(user.isAdmin === true)
                ? <AdminView checkoutData={checkout} fetchData={fetchData}/>
                : <UserView checkoutData={checkout}/>
            }
        </Fragment>
    )

}