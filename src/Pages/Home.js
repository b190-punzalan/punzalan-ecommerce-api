import {Fragment} from 'react';


import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Quote from '../components/Quote';
import Courses from './Courses';



export default function Home(){
	const data = {
		title:"Raketera Mama",
		content: "Making ends meet, growing money, and finding joy in it. Let's raket 'til we make it!",
		destination: "/courses",
		label: "Enroll Now!"
	}
	
	return(
		<Fragment>
			<Banner data={data}/>
        	<Highlights/>
        	<Quote/>
        	<Courses/>
        </Fragment>

	)
}

