import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){

	const { user } = useContext(UserContext);
	const navigate = useNavigate();

		
		const [firstName, setFirstName] = useState('');
		const [lastName, setLastName] = useState('');
		const [email, setEmail] = useState('');
		const [mobileNumber, setMobileNumber] = useState('');
		const [username, setUsername] = useState('');
		const [password1, setPassword1] = useState('');
		const [password2, setPassword2] = useState('');
		// state to determine whether the submit button is enabled or not
		const [isActive, setIsActive] = useState(false);

		
		console.log(email);
		console.log(username);
		console.log(password1);
		console.log(password2);
		
	useEffect(()=>{
		if( ( firstName !== '' && lastName !== '' && mobileNumber.length === 11 && email !== '' && username !== '' && password1 !== '' && password2 !== '' ) && ( password1 === password2 ) ){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [ firstName, lastName, mobileNumber, email, username, password1, password2 ]);

	function registerUser(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
		    method: "POST",
		    headers: {
		        'Content-Type': 'application/json'
		    },
		    body: JSON.stringify({
		        email: email
		    })
		})
		.then(res => res.json())
		.then(data => {

		    console.log(data);

		    if(data === true){
		    	Swal.fire({
		    		title: "Duplicate	email found",
		    		icon: "error",
		    		text: "Kindly provide another email address."
		    	})
		    }else{
		    	fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
		    	    method: "POST",
		    	    headers: {
		    	        'Content-Type': 'application/json'
		    	    },
		    	    body: JSON.stringify({
		    	        firstName: firstName,
		    	        lastName: lastName,
		    	        email: email,
		    	        mobileNumber: mobileNumber,
		    	        username: username,
		    	        password: password1
		    	    })
		    	})
		    	.then(res => res.json())
		    	.then(data => {

		    	    console.log(data);

		    	    if (data === true) {

		    	        // Clear input fields
		    	        setFirstName('');
		    	        setLastName('');
		    	        setEmail('');
		    	        setMobileNumber('');
		    	        setUsername('');
		    	        setPassword1('');
		    	        setPassword2('');

		    	        Swal.fire({
		    	            title: 'Registration successful',
		    	            icon: 'success',
		    	            text: `Welcome, ${username}! Raketera Mama is excited to start this journey with you!`

		    	        });
	    	            navigate('/login')

		    	    } else {

		    	        Swal.fire({
		    	            title: 'Something wrong',
		    	            icon: 'error',
		    	            text: 'Please try again.'   
		    	        });

		    	    }
		    	})
		    }
		})
	}

	return (
		(user.id !== null) ?
		<Navigate to='/courses' />
		:
	  <Form onSubmit={(e) => registerUser(e)}>
	    <Form.Group controlId="userEmail">
	      <Form.Label>First Name</Form.Label>
	      <Form.Control 
	      				type="text" 
	      				placeholder="Enter first name" 
	      				value={firstName}
	      				onChange={e => setFirstName(e.target.value)}
	      				required 
			/>
	      
	    </Form.Group>

	    <Form.Group controlId="userEmail">
	      <Form.Label>Last Name</Form.Label>
	      <Form.Control 
	      				type="text" 
	      				placeholder="Enter last name" 
	      				value={lastName}
	      				onChange={e => setLastName(e.target.value)}
	      				required 
			/>
	      
	    </Form.Group>

	    <Form.Group controlId="userEmail">
	      <Form.Label>Mobile Number</Form.Label>
	      <Form.Control 
	      				type="text" 
	      				placeholder="Enter mobile number" 
	      				value={mobileNumber}
	      				onChange={e => setMobileNumber(e.target.value)}
	      				required 
			/>
	      
	    </Form.Group>

	    <Form.Group controlId="userEmail">
	      <Form.Label>Email address</Form.Label>
	      <Form.Control 
	      				type="email" 
	      				placeholder="Enter email" 
	      				value={email}
	      				onChange={e => setEmail(e.target.value)}
	      				required 
			/>
	      <Form.Text className="text-muted">
	        We'll never share your email with anyone else.
	      </Form.Text>
	    </Form.Group>

	    <Form.Group controlId="userEmail">
	      <Form.Label>Username</Form.Label>
	      <Form.Control 
	      				type="text" 
	      				placeholder="Enter username" 
	      				value={username}
	      				onChange={e => setUsername(e.target.value)}
	      				required 
			/>
	      
	    </Form.Group>

	    <Form.Group controlId="password1">
	      <Form.Label>Password</Form.Label>
	      <Form.Control
	      				type="password" 
	      				placeholder="Password" 
	      				value={password1}
	      				onChange={e => setPassword1(e.target.value)}
	      				required
			/>
	    </Form.Group>

	    <Form.Group controlId="password2">
	      <Form.Label>Verify Password</Form.Label>
	      <Form.Control 
	      				type="password" 
	      				placeholder="Password" 
	      				value={password2}
	      				onChange={e => setPassword2(e.target.value)}
	      				required
			/>
	    </Form.Group>
	    {isActive ?
	    <Button variant="primary" type="submit" id="submitBtn">Register</Button>
	    :
	    <Button variant="primary" type="submit" id="submitBtn" disabled>Register</Button>
		}
	  </Form>
	);
}