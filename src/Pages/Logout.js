import { Navigate } from 'react-router-dom';
import {useContext, useEffect} from 'react';
import UserContext from '../UserContext';

export default function Logout() {
	// consumes the UserContext object and destructure t to access the unsetUser and setUser states
	const {unsetUser, setUser} = useContext(UserContext);

// clear the local storage:
	unsetUser();

// placing the "setUser" setter function inside of a useEffect is necessary because of the updates in React JS that a state of another component cannot be updated while trying to render a different component:
	useEffect(()=>{
		setUser({id: null});
	})

	return(
		<Navigate to='/login' />
	)
}
