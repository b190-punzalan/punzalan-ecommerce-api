import { Fragment, useEffect, useState, useContext } from 'react';
import AdminView from '../components/AdminView';
// import coursesData from '../data/coursesData';
import UserView from '../components/UserView';
import UserContext from '../UserContext';



export default function Courses(){
	
	const { user } = useContext(UserContext);

	const [courses, setCourses] = useState([]);

	const fetchData = () => {

        fetch(`${ process.env.REACT_APP_API_URL}/courses/all`)
        .then(res => res.json())
        .then(data => {

            setCourses(data);

        });

    }


	useEffect(() => {
        fetchData();
    }, []);
    
    return (
    	
        <Fragment>
            {/* If the user is an admin, show the Admin View. If not, show the regular courses page. */}
            {(user.isAdmin === true)
                ? <AdminView coursesData={courses} fetchData={fetchData}/>
                : <UserView coursesData={courses}/>
            }
        </Fragment>
    )

}

	